#include "44b.h"
#include "gpio.h"

/* Port B interface implementation */

/* BIT OPERATIONS MACROS */
// Bit Clear (BIC in ARM v4T instruction set)
#define B20(port, bit) port &= ~(0b1 << (bit))
// Bit Set
#define B21(port, bit) port |= (0b1 << (bit))
// Bit Check
#define BC(port, bit) port & (0b1 << (bit))

int portB_conf(int pin, enum port_mode mode)
{
	int ret = 0;
	if (pin < 0 || pin > 10)
		return -1; // indica error

	if (mode == SIGOUT)
		// BIT SET
		B21(rPCONB, pin);
	else if (mode == OUTPUT)
		// BIT CLEAR (BIC rd, ra, rb)
		B20(rPCONB, pin);
	else
		ret = -1; // indica error

	return ret;
}

int portB_write(int pin, enum digital val)
{
	if (pin < 0 || pin > 10)
		return -1; // indica error

	if (val < 0 || val > 1)
		return -1; // indica error

	if (val)
		B21(rPDATB, pin);
	else
		B20(rPDATB, pin);

	return 0;
}

/* Port G interface implementation */

int portG_conf(int pin, enum port_mode mode)
{
	int pos  = pin*2;

	if (pin < 0 || pin > 7)
		return -1; // indica error

	switch (mode) {
		case INPUT:
			// poner en rPCONG 00 a partir de la posición pos para
			// configurar como pin de entrada el pin indicado por el parámetro pin
			B20(rPCONG, pos);
			B20(rPCONG, pos+1);
			break;
		case OUTPUT:
			// poner en rPCONG 01 a partir de la posición pos para
			// configurar como pin de salida el pin indicado por el parámetro pin
			B20(rPCONG, pos);
			B21(rPCONG, pos+1);
			break;
		case SIGOUT:
			// poner en rPCONG 10 a partir de la posición pos para
			// que salga la señal interna correspondiente por el pin indicado
			// por el parámetro pin
			B21(rPCONG, pos);
			B20(rPCONG, pos+1);
			break;
		case EINT:
			// poner en rPCONG 11 a partir de la posición pos para
			// habilitar la generación de interrupciones externas por el pin
			// indicado por el parámetro pin
			B21(rPCONG, pos);
			B21(rPCONG, pos+1);
			break;
		default:
			return -1;
	}

	return 0;
}

int portG_eint_trig(int pin, enum trigger trig)
{
	// A COMPLETAR EN LA SEGUNDA PARTE DE LA PRÁCTICA
	return 0;
}

int portG_write(int pin, enum digital val)
{
	int pos = pin*2;

	if (pin < 0 || pin > 7)
		return -1; // indica error

	if (val < 0 || val > 1)
		return -1; // indica error

	if ((rPCONG & (0x3 << pos)) != (0x1 << pos))
		return -1; // indica error

	if (val)
		// poner en rPDATG el bit indicado por pin a 1
		B21(rPDATG, pin);
	else
		// poner en rPDATG el bit indicado por pin a 0
		B20(rPDATG, pin);
	return 0;
}

int portG_read(int pin, enum digital* val)
{
	int pos = pin*2;

	if (pin < 0 || pin > 7)
		return -1; // indica error

	if (rPCONG & (0x3 << pos))
		return -1; // indica error

	if (BC(rPDATG, pin))
		*val = HIGH;
	else
		*val = LOW;

	return 0;
}

int portG_conf_pup(int pin, enum enable st)
{
	if (pin < 0 || pin > 7)
		return -1; // indica error

	if (st != ENABLE && st != DISABLE)
		return -1; // indica error

	if (st == ENABLE)
		// poner el pin de rPUPG indicado por el parametro pin al valor adecuado,
		// para activar la resistencia de pull-up
		B21(rPUPG, pin);
	else
		// poner el pin de rPUPG indicado por el parametro pin al valor adecuado,
		// para desactivar la resistencia de pull-up
		B20(rPUPG, pin);
	return 0;
}

